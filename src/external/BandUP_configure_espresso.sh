#! /bin/sh

rm -rf espresso-5.1_modules_for_BandUP
tar -xvzf espresso-5.1_modules_for_BandUP.tgz
cd espresso-5.1_modules_for_BandUP

./configure FC=${1:-gfortran} CC=${2:-gcc} ${3:---enable-openmp} ${4:---disable-parallel}

make bandup
